module.exports.protobufType = {
  "nested": {
    "Error": {
      "fields": {
        "hash":           {"type": "string", "id": 1 },
        "objType":        {"type": "string", "id": 2 },
        "objName":        {"type": "string", "id": 3 },
        "objSubName":     {"type": "string", "id": 4 },
        "createdBy":      {"type": "string", "id": 5 },
        "created":        {"type": "int32",  "id": 6 },
        "lastUpdBy":      {"type": "string", "id": 7 },
        "lastUpd":        {"type": "int32",  "id": 8 },
        "errorOwner":     {"type": "string", "id": 9 },
        "errorDate":      {"type": "int32",  "id": 10 },
        "ruleId":         {"type": "string", "id": 11 },
        "message":        {"type": "string", "id": 12 },
        "line":           {"type": "int32",  "id": 20 },
        "column":         {"type": "int32",  "id": 21 },
        "endLine":        {"type": "int32",  "id": 22 },
        "endColumn":      {"type": "int32",  "id": 23 },
        "srcCodePrefix":  {"type": "string", "id": 24 },
        "srcCode":        {"type": "string", "id": 25 },
        "srcCodePostfix": {"type": "string", "id": 26 },
        "nodeType":       {"type": "string", "id": 27 },
        "hiddenDate":     {"type": "int32",  "id": 30 },
        "hiddenUntil":    {"type": "int32",  "id": 31 },
        "hiddenComment":  {"type": "string", "id": 32 }
      }
    }
  }
};