/* global window */

const protobuf = require('protobufjs');
const speedDateFormatter = require('speed-date')('YYYY-MM-DD');
const protobufTypeM = require('./protobufType.js');
const protobufType = protobufTypeM.protobufType;
const ErrorProtobufType = protobuf.Root.fromJSON( protobufType ).lookup('Error');

const isBrowser = typeof window  !== 'undefined' && typeof window.document !== 'undefined';
const isNode    = typeof process !== 'undefined' && process.versions !== null &&  typeof process.versions.node !== 'undefined' && process.versions.node !== null;

const logger = process.env.LOGGER || console;

var crypto = require('crypto');

var cache = {
  data:false
};

class Errors{
  static GetErrors(id) {
    if (cache.hasOwnProperty(id)) {
        if(! cache[id]) cache[id] = new Errors(id);
        return cache[id];
    }
    return new Errors(id);
  }

  constructor(id){
    this.whereList = [];
    this.countAll  = 0;
    this.records   = [];

    // init Errors from parent
    if(typeof id==='object'){
      if(Array.isArray(id.whereList)){
        this.parent = id.parent || id;
        this.whereList = id.whereList.slice();
        return;
      }
      throw Error('if object, then wait Error implementation');
    }

    if(typeof id==='string') {
      if (cache.hasOwnProperty(id)) {
        this.id = id;
        return;
      }
      throw Error('invalid id');
    }

    if(typeof id==='number') {
      this.id = id;
      return;
    }

    if(typeof id==='undefined') return;

    throw Error('wait arg object or string or number or undefined');
  }

  forEach(f) {
    this.records.forEach(f);
  }
    
  get countFiltered(){
    return this.records.length;
  }

  async save(){
    if(!isNode) throw Error('save function work only in node');
    if ((typeof this.id !== 'undefined') && (this.id !== 'data'))
      throw new Error('save can only rewrite data file');

    const fs = require('fs');

    var writer = protobuf.Writer.create();
    this.forEach((err) => ErrorProtobufType.encodeDelimited(err, writer));
    var errFileBuffer = writer.finish();

    if(! fs.existsSync(process.env.DATAPATH)) {
      fs.mkdirSync(process.env.DATAPATH);
    }

    if (typeof this.id === 'undefined'){ // before writeFileSync because need change id
      this.id = 'data';
      cache['data'] = this;
    }

    try{
      fs.writeFileSync(process.env.DATAPATH+'/'+this.id+'.protobuf', errFileBuffer);
      logger.debug('save '+ this.id);
    }catch(e){
      logger.error('dont save '+this.id+' Errors. '+e);
    }
  }

  async load(reload){
    if ((this.records.length>0)&&(!reload)) return this;
    this.records = [];
    this.countAll = 0;

    var errFileBuffer;

    if(isNode){
      const fs = require('fs');
      try {
        errFileBuffer = fs.readFileSync(process.env.DATAPATH+'/'+this.id+'.protobuf');
      } catch (unused) {
        return;
      }
    }

    if(isBrowser){
      try {
        let url = process.env.PUBLIC_URL+'/data/'+this.id+'.protobuf';
        logger.debug(url);
        let res = await fetch(url);
        errFileBuffer = new Uint8Array(await(res).arrayBuffer());
      } catch (unused) {
        return;
      }
    }

    var reader = protobuf.Reader.create(errFileBuffer);
    while (reader.pos < reader.len) {
      let err = ErrorProtobufType.decodeDelimited(reader);
      this.add(err);
    }

    return this;
  }
  
  async filter(conditions) {
    if(Array.isArray(conditions)){
      conditions.forEach(c => this.whereList.push(c));
    } else {
      this.whereList.push(conditions);
    }

    let oldRecords = this.records;
    this.records = [];
    for(var i = 0; i< oldRecords.count; ++i) {
      if( await this.suitable(oldRecords[i]) ) {
        this.records.push( oldRecords[i] );
      }
    }

    return this;
  }

  async suitable(err) {
    for(var i = 0; i < this.whereList.length; ++i){
      let item = this.whereList[i];
      if (item==='not hidden') {
        if (err.hiddenUntil > Date.now()/1000) return false;
      } else {
        switch (item.operator) {
          case '>':  if(! (err[item.field] >   item.value))          {return false;} break;
          case '<':  if(! (err[item.field] <   item.value))          {return false;} break;
          case '=':  if(! (err[item.field] === item.value))          {return false;} break;
          case 'in': if(! (item.value.indexOf(err[item.field])>-1 )) {return false;} break;
          default: return false;
        }
      }
    }
    return true;
  }

  static async hide(hash, hiddenUntil, hiddenComment){
    if(isBrowser)
      throw new Error('not implemented');

    if(isNode) {
      let data = Errors.GetErrors('data');
      await data.load();
      for(let i = 0; i < data.records.length; ++i ){
        let err = data.records[i];
        if(err.hash === hash) {
          err.hiddenDate  = err.hiddenDate > 0 ? err.hiddenDate : Date.now() / 1000 ;
          err.hiddenUntil = hiddenUntil > 0 ? hiddenUntil : err.hiddenUntil;
          err.hiddenComment = hiddenComment;
          logger.debug('update item with hash '+hash);
          await data.save();
          return {msg: 'data saved', data: err};
        }
      }
      logger.error('not found item with hash '+hash);
      return {msg: 'not found item with hash '+hash, data: {hash}};
    }
    throw new Error('not isBrowser and not isNode');
  }

  static dateFormat(date){
    return date>0 ? speedDateFormatter( new Date( date * 1000)) : '';
  }

  static updateStr(err){
    if(isBrowser) { //don't need calc in node, because don't need big file size
      err.createdStr = Errors.dateFormat(err.created);
      err.lastUpdStr = Errors.dateFormat(err.lastUpd);
      err.errorDateStr = Errors.dateFormat(err.errorDate);
      err.hiddenDateStr = Errors.dateFormat(err.hiddenDate);
      err.hiddenUntilStr = Errors.dateFormat(err.hiddenUntil);
    }
  }

  add(err){
    ++this.countAll;

    Errors.updateStr(err);

    if((typeof err.hash !== "string") || (err.hash===""))
      Errors.setHash(err);

    this.records.push( err );
  }

  static setHash(err) {
    let sha1 = crypto.createHash('sha1');
    sha1.update(err.objType +'//'+ err.objName +'//'+ err.objSubName +'//'
              + (typeof err.ruleId === 'undefined' ? '' : err.ruleId) +'//'
              + (typeof err.line   === 'undefined' ? '' : err.line) +'//'
              + (typeof err.column === 'undefined' ? '' : err.column) +'//'
              + err.message +'//'+ err.srcCode);
    err.hash = sha1.digest('base64');
  }

  static setHashWithoutLine(err) {
    let sha1 = crypto.createHash('sha1');
    sha1.update(err.objType +'//'+ err.objName +'//'+ err.objSubName +'//'
              + (typeof err.ruleId === 'undefined' ? '' : err.ruleId) +'//'
              + (typeof err.column === 'undefined' ? '' : err.column) +'//'
              + err.message +'//'+ err.srcCode);
    err.hashWithoutLine = sha1.digest('base64');
  }

  initHashWithoutLine() {
    for(var i=0; i<this.records.length; ++i)
      Errors.setHashWithoutLine(this.records[i]);
  }

  distinct(field){
    var result = [];
    for(var i=0; i<this.records.length; ++i){
      if(result.indexOf(this.records[i][field])===-1){
        result.push(this.records[i][field]);
      }
    }
    return result;
  }

  findSimilar(err, forecastIndex = 0) {
    if (typeof err === "undefined") {
      logger.error("hash not difined");
      throw Error("hash not difined");
    }
    
    if ((typeof err.hash !=="string") || (err.hash ==="")) {
      logger.error("hash not difined");
      throw Error("hash not difined");
    }

    let checkHash = (i) => {
      return ((typeof this.records[forecastIndex+i] !== 'undefined') 
        && (this.records[forecastIndex+i].hash === err.hash))
    };

    let checkSimilar = (i, similar) => {
      if ( (typeof this.records[forecastIndex+i] !== 'undefined')
        && (this.records[forecastIndex+i].hashWithoutLine === err.hashWithoutLine)
        && ((typeof similar === 'undefined')
            || (    Math.abs(err.line-this.records[forecastIndex+i].line) 
                  < Math.abs(err.line-similar.line)                       )
           )
         )
         return this.records[forecastIndex+i];
      else return similar;
    };

    let similar = undefined;
    for(let i = 0; i<this.records.length; ++i){
      if ( checkHash( i) ) return this.records[forecastIndex+i];
      if ( checkHash(-i) ) return this.records[forecastIndex-i];
      
      similar = checkSimilar( i, similar);
      similar = checkSimilar(-i, similar);
    }
    return similar;
  }

  async populateOwnerDateAndHide(predefineLast){
    logger.debug('populateOwnerDateAndHide');
    let startTime = Date.now();
    if(typeof this.id !== 'undefined') 
      throw new Error('only for new errors i can load last errors list');

    let last = predefineLast || Errors.GetErrors('data');
    await last.load(!predefineLast);
    this.initHashWithoutLine();
    last.initHashWithoutLine();
    for(var i=0; i< this.records.length; ++i){
      let found = last.findSimilar(this.records[i], i);
      if(found){
        this.records[i].errorOwner    = found.errorOwner;
        this.records[i].errorDate     = found.errorDate;
        this.records[i].hiddenDate    = found.hiddenDate;
        this.records[i].hiddenUntil   = found.hiddenUntil;
        this.records[i].hiddenComment = found.hiddenComment;
      }
      if(!this.records[i].errorOwner) {
        this.records[i].errorOwner = this.records[i].lastUpdBy ? this.records[i].lastUpdBy : this.records[i].createdBy;
      }
      if(!this.records[i].errorDate) {
        this.records[i].errorDate  = this.records[i].lastUpd ? this.records[i].lastUpd : Date.now() / 1000;
      }
    }
    logger.debug('populateOwnerDateAndHide time '+(Date.now()-startTime)/100+' seconds');
  }
}

module.exports.Errors = Errors;