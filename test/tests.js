/* global describe,it */

const assert = require('assert');
const Errors = require('../Errors').Errors;
describe('Core.Errors', function() {
  describe('hash()', function() {    
    it('different hash for different srcCode', async function() {
        let testHash = new Errors();

        const errorTemplate = {objType: 't', objName: 'n', objSubName: 's', createdBy: 'c', created: 1, ruleId: 'r', column: 1, endLine: 9, endColumn: 0, srcCodePrefix: 'pr', srcCodePostfix: 'po',nodeType:  '', lastUpdBy: 'u', lastUpd: 2, message: 'm1', line: 11, errorOwner: 'o1', errorDate: 7771, hiddenDate: 81, hiddenUntil: 91, hiddenComment: 'h1'};

        e1 = Object.assign({}, errorTemplate, {srcCode: 'srcCode1'} );
        e2 = Object.assign({}, errorTemplate, {srcCode: 'srcCode2'} );
        testHash.add(e1);
        testHash.add(e2);

        assert.notEqual(e1.hash, e2.hash);
    })


    it('check hash calc for VhXLG+C70MZC9aan7cgZdITAmrA=', function() {
      let testHash = new Errors();

      const error = {
        objType: 'Business component', 
        objName: 'Action', 
        objSubName: 'BusComp_PreSetFieldValue', 
        ruleId: 'explicit-object-release', 
        message: 'Object assessInp is not reset to null.',
        column: 8,
        line: 10,
        srcCode: 'var assessInp:PropertySet = TheApplication().NewPropertySet();'
      };
        
      testHash.add(error);
      
      assert.equal(error.hash, 'VhXLG+C70MZC9aan7cgZdITAmrA=');
    })
  });
  describe('populateOwnerDateAndHide()', function() {
    it('general test', async function() {
      var lastErrors = new Errors();
      var newErrors  = new Errors();

      const errorTemplate = {objType: 't', objName: 'n', objSubName: 's', createdBy: 'c', created: 1, 
                ruleId: 'r', column: 1, endLine: 9, endColumn: 0, srcCodePrefix: 'pr', srcCodePostfix: 'po',nodeType:  ''};

      lastErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm1', line: 11, srcCode: 'srcCode1', errorOwner: 'o1', errorDate: 7771, hiddenDate: 81, hiddenUntil: 91, hiddenComment: 'h1'} ));
      lastErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm2', line: 12, srcCode: 'srcCode2', errorOwner: 'o2', errorDate: 7772, hiddenDate: 82, hiddenUntil: 92, hiddenComment: 'h2'} ));
      lastErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm1', line: 13, srcCode: 'srcCode3', errorOwner: 'o3', errorDate: 7773, hiddenDate: 83, hiddenUntil: 93, hiddenComment: 'h3'} ));
      lastErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm2', line: 14, srcCode: 'srcCode4', errorOwner: 'o4', errorDate: 7774, hiddenDate: 84, hiddenUntil: 94, hiddenComment: 'h4'} ));
      // полностью совпадает с 0
      newErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm1', line: 11, srcCode: 'srcCode1'} ));
      // полностью совпадает с 1, кроме lastUpdate
      newErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'uu', lastUpd: 22, message: 'm2', line: 12, srcCode: 'srcCode2'} ));
      // отличается от 2 только номером строки
      newErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm1', line: 133, srcCode: 'srcCode3'} ));
      // отличается от 3 только исходным кодом (не должна биться)
      newErrors.add(Object.assign({},errorTemplate, {lastUpdBy: 'u', lastUpd: 2, message: 'm2', line: 14, srcCode: 'srcCode444'} ));

      await newErrors.populateOwnerDateAndHide(lastErrors);

      let multiCheck = (e1, e2, msg) => {
        assert.equal(e1.errorOwner    , e2.errorOwner   , msg);
        assert.equal(e1.errorDate     , e2.errorDate    , msg);
        assert.equal(e1.hiddenDate    , e2.hiddenDate   , msg);
        assert.equal(e1.hiddenUntil   , e2.hiddenUntil  , msg);
        assert.equal(e1.hiddenComment , e2.hiddenComment, msg);
      }

      multiCheck(newErrors.records[0], lastErrors.records[0], "rec0");
      multiCheck(newErrors.records[1], lastErrors.records[1], "rec1");
      multiCheck(newErrors.records[2], lastErrors.records[2], "rec2");

      assert.equal(newErrors.records[3].errorOwner    , newErrors.records[3].lastUpdBy);
      assert.equal(newErrors.records[3].errorDate     , newErrors.records[3].lastUpd);
      assert.equal(newErrors.records[3].hiddenDate    , undefined);
      assert.equal(newErrors.records[3].hiddenUntil   , undefined);
      assert.equal(newErrors.records[3].hiddenComment , undefined);
    });

    it('reload test', async function() {
      const errorTemplate = {
        objType: 'Business component', 
        objName: 'Action', 
        objSubName: 'BusComp_PreSetFieldValue', 
        ruleId: 'explicit-object-release', 
        message: 'Object assessInp is not reset to null.',
        column: 8,
        line: 10,
        srcCode: 'var assessInp:PropertySet = TheApplication().NewPropertySet();'
      };

      process.env.DATAPATH = './test/data';
      let lastErrors = new Errors();
      lastErrors.add(Object.assign({},errorTemplate, {hiddenDate: 777, hiddenUntil: 7777, hiddenComment: 'h1'} ));
      await lastErrors.save();

      lastErrors.records[0].hiddenDate   = 0;
      lastErrors.records[0].hiddenUntil  = 0;
      lastErrors.records[0].hiddenComment = '';
      
      let newErrors = new Errors();
      newErrors.add(Object.assign({},errorTemplate));
      await newErrors.populateOwnerDateAndHide();

      assert.equal(newErrors.records[0].hiddenDate   , 777);
      assert.equal(newErrors.records[0].hiddenUntil  , 7777);
      assert.equal(newErrors.records[0].hiddenComment, 'h1');
    });
  });
});